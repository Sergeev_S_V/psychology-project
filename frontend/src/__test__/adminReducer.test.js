import merge from 'xtend';
import reducer from '../store/reducers/adminReducer';
import {initialState} from '../store/reducers/adminReducer';
import {
    EDIT_INFO_FAILURE,
    FETCH_ALL_USERS_ERROR, FETCH_ALL_USERS_SUCCESS, FETCH_FULL_INFO_USER_FAILURE, FETCH_FULL_INFO_USER_SUCCESS,
    FETCH_NOTIFICATIONS_ADMIN_SUCCESS, FETCH_NOTIFICATIONS_SUCCESS, GET_ABOUT_INFO, GET_ABOUT_INFO_FAILURE,
    GET_IMPORTANT_USERS,
    GET_PSYCHOLOGISTS_FAILURE,
    LOGOUT_USER, NOTIFY_ADMIN_FAILURE
} from "../store/actions/actionTypes";

describe('admin reducer', () => {

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle LOGOUT_USER', () => {
        const action = {type: LOGOUT_USER, allUsers: null, currentUser: null};
        const expectedState = merge(initialState,
            {allUsers: null, currentUser: null});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_ALL_USERS_SUCCESS', () => {
        const action = {type: FETCH_ALL_USERS_SUCCESS, users: [{id: '123', email: 'testUser@mail.ru', role: 'user'}]};
        const expectedState = merge(initialState,
            {allUsers: [{id: '123', email: 'testUser@mail.ru', role: 'user'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_ALL_USERS_ERROR', () => {
        const action = {type: FETCH_ALL_USERS_ERROR, error: 'FETCH_ALL_USERS_ERROR'};
        const expectedState = merge(initialState,
            {error: 'FETCH_ALL_USERS_ERROR'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_FULL_INFO_USER_SUCCESS', () => {
        const action = {type: FETCH_FULL_INFO_USER_SUCCESS, user: {id: '123', email: 'admin@mail.ru', role: 'admin', displayName: 'Admin'}};
        const expectedState = merge(initialState,
            {currentUser: {id: '123', email: 'admin@mail.ru', role: 'admin', displayName: 'Admin'}});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_FULL_INFO_USER_FAILURE', () => {
        const action = {type: FETCH_FULL_INFO_USER_FAILURE, error: 'FETCH_FULL_INFO_USER_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'FETCH_FULL_INFO_USER_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle NOTIFY_ADMIN_FAILURE', () => {
        const action = {type: NOTIFY_ADMIN_FAILURE, error: 'NOTIFY_ADMIN_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'NOTIFY_ADMIN_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_NOTIFICATIONS_ADMIN_SUCCESS', () => {
        const action = {type: FETCH_NOTIFICATIONS_ADMIN_SUCCESS, notifications: [{id: '123', userId: '456', 'sectionId': '789', notify: true}]};
        const expectedState = merge(initialState,
            {notifications: [{id: '123', userId: '456', 'sectionId': '789', notify: true}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_NOTIFICATIONS_SUCCESS', () => {
        const action = {type: FETCH_NOTIFICATIONS_SUCCESS, importantData: [{id: '123', userId: '456', 'sectionId': '789', notify: true}]};
        const expectedState = merge(initialState,
            {importantNotifications: [{id: '123', userId: '456', 'sectionId': '789', notify: true}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_IMPORTANT_USERS', () => {
        const action = {type: GET_IMPORTANT_USERS, data: [{id: '123', email: 'testUser@mail.ru', role: 'user'}]};
        const expectedState = merge(initialState,
            {importantUsers: [{id: '123', email: 'testUser@mail.ru', role: 'user'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_PSYCHOLOGISTS_FAILURE', () => {
        const action = {type: GET_PSYCHOLOGISTS_FAILURE, error: 'GET_PSYCHOLOGISTS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_PSYCHOLOGISTS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_ABOUT_INFO', () => {
        const action = {type: GET_ABOUT_INFO, data: {title: 'Title', about: 'About'}};
        const expectedState = merge(initialState,
            {about: {title: 'Title', about: 'About'}});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_ABOUT_INFO_FAILURE', () => {
        const action = {type: GET_ABOUT_INFO_FAILURE, error: 'GET_ABOUT_INFO_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_ABOUT_INFO_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle EDIT_INFO_FAILURE', () => {
        const action = {type: EDIT_INFO_FAILURE, error: 'EDIT_INFO_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'EDIT_INFO_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});