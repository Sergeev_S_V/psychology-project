import moxios from 'moxios';
import axios from "../axios";

export default {
  expectData: function(expectedData, status = 200) {
    moxios.wait(() => {
      const request = moxios.requests.mostRecent();
      request.respondWith({
        status: status,
        response: expectedData,
      });
    });
  },
  install: function() {
    beforeEach(function () {
      moxios.install(axios);
    });
  },
  uninstall: function() {
    afterEach(function () {
      moxios.uninstall(axios);
    });
  }
}