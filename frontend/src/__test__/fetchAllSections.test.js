import {FETCH_ALL_SECTIONS_SUCCESS} from "../store/actions/actionTypes";
import {fetchAllSection} from "../store/actions/sectionActions";

import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import moxios from 'moxios';
import expect from 'expect';
import axios from "../axios";

let middleware = [thunk];
const mockStore = configureMockStore(middleware);



describe('Testing fetchAllSectionsAction', () => {
    beforeEach(function () {
        moxios.install(axios);
    });

    afterEach(function () {
        moxios.uninstall(axios);
    });


    it('creates FETCH_ALL_SECTIONS_SUCCESS when fetching sections has been done', done => {
        const allSections = [
            {title: 'section 1'},
            {title: 'section 2'},
            {title: 'section 3'},
        ];

        moxios.wait(() => {
            const request = moxios.requests.mostRecent();
            request.respondWith({
                status: 200,
                response: allSections,
            });
        });

        const expectedActionsArray = [{type: FETCH_ALL_SECTIONS_SUCCESS, allSections}];
        const store = mockStore({});


        store.dispatch(fetchAllSection())
            .then(() => {
                expect(store.getActions()).toEqual(expectedActionsArray);
                done();
            });
    })

});