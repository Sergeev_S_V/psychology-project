import merge from 'xtend';
import reducer from '../store/reducers/contactsReducer';
import {initialState} from '../store/reducers/contactsReducer';
import {
    EDIT_CONTACTS_FAILURE,
    GET_CONTACT_ID_FAILURE, GET_CONTACT_ID_SUCCESS, GET_CONTACTS_FAILURE,
    GET_CONTACTS_SUCCESS
} from "../store/actions/actionTypes";

describe('contacts reducer', () => {

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_CONTACTS_SUCCESS', () => {
        const action = {type: GET_CONTACTS_SUCCESS, data: [{phone: '0555123456', whatsapp: '0555123456', facebook: 'testFacebook', instagram: 'testInsta', address: 'testAddress'}]};
        const expectedState = merge(initialState,
            {contacts: [{phone: '0555123456', whatsapp: '0555123456', facebook: 'testFacebook', instagram: 'testInsta', address: 'testAddress'}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_CONTACT_ID_SUCCESS', () => {
        const action = {type: GET_CONTACT_ID_SUCCESS, data: {id: '123'}};
        const expectedState = merge(initialState,
            {idContact: {id: '123'}});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_CONTACTS_FAILURE', () => {
        const action = {type: GET_CONTACTS_FAILURE, error: 'GET_CONTACTS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_CONTACTS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_CONTACT_ID_FAILURE', () => {
        const action = {type: GET_CONTACT_ID_FAILURE, error: 'GET_CONTACT_ID_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_CONTACT_ID_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle EDIT_CONTACTS_FAILURE', () => {
        const action = {type: EDIT_CONTACTS_FAILURE, error: 'EDIT_CONTACTS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'EDIT_CONTACTS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});