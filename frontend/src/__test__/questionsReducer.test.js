import merge from 'xtend';
import reducer from '../store/reducers/questionsReducer';
import {initialState} from '../store/reducers/questionsReducer';
import {
    ANALYSE_TEST_RESULTS_SUCCESS,
    GET_QUESTION_BY_ID_SUCCESS,
    ADD_TEST_QUESTION_FAILURE,
    ANALYSE_TEST_RESULTS_FAILURE, GET_QUESTION_BY_ID_FAILURE,
    DELETE_QUESTION_FAILURE, EDIT_QUESTION_FAILURE, ADD_QUESTION_FAILURE
} from '../store/actions/actionTypes';

describe('questions reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle GET_QUESTION_BY_ID_SUCCESS', () => {
        const action = {type: GET_QUESTION_BY_ID_SUCCESS, data: {_id: '123', questions: [{id: '1'}, {id: '2'}, {id: '3'}, {id: '4'}]}};
        const expectedState = merge(initialState,
            {sectionId: '123', questions: [{id: '1'}, {id: '2'}, {id: '3'}, {id: '4'}]});
        expect(reducer(initialState, action)).toEqual(expectedState)
    });

    it('should handle ANALYSE_TEST_RESULTS_SUCCESS', () => {
        const action = {type: ANALYSE_TEST_RESULTS_SUCCESS, testResults: [{
            title: 'Шкала социальной желательности',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Общая удовлетворенность собой',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Поведение',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Интеллект, положение в школе',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Ситуация в школе',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Внешность, физическая привлекательность, физическое развитие как свойства, связанные с популярностью среди сверстников',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Тревожность',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Общение,популярность среди сверстников, умение общаться',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Счастье и удовлетворенность',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Положение в семье',
            score: 90,
            interpretation: 'Some string'
        }, {
            title: 'Уверенность в себе',
            score: 90,
            interpretation: 'Some string'
        }]};

        const expectedState = merge(initialState,
            {testResults: [{
                    title: 'Шкала социальной желательности',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Общая удовлетворенность собой',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Поведение',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Интеллект, положение в школе',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Ситуация в школе',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Внешность, физическая привлекательность, физическое развитие как свойства, связанные с популярностью среди сверстников',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Тревожность',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Общение,популярность среди сверстников, умение общаться',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Счастье и удовлетворенность',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Положение в семье',
                    score: 90,
                    interpretation: 'Some string'
                }, {
                    title: 'Уверенность в себе',
                    score: 90,
                    interpretation: 'Some string'
                }
            ]});

        expect(reducer(initialState, action)).toEqual(expectedState)
    });

    it('should handle ADD_TEST_QUESTION_FAILURE', () => {
        const action = {type: ADD_TEST_QUESTION_FAILURE, error: 'ADD_TEST_QUESTION_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'ADD_TEST_QUESTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle ANALYSE_TEST_RESULTS_FAILURE', () => {
        const action = {type: ANALYSE_TEST_RESULTS_FAILURE, error: 'ANALYSE_TEST_RESULTS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'ANALYSE_TEST_RESULTS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_QUESTION_BY_ID_FAILURE', () => {
        const action = {type: GET_QUESTION_BY_ID_FAILURE, error: 'GET_QUESTION_BY_ID_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_QUESTION_BY_ID_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle DELETE_QUESTION_FAILURE', () => {
        const action = {type: DELETE_QUESTION_FAILURE, error: 'DELETE_QUESTION_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'DELETE_QUESTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle EDIT_QUESTION_FAILURE', () => {
        const action = {type: EDIT_QUESTION_FAILURE, error: 'EDIT_QUESTION_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'EDIT_QUESTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle ADD_QUESTION_FAILURE', () => {
        const action = {type: ADD_QUESTION_FAILURE, error: 'ADD_QUESTION_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'ADD_QUESTION_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState)
    });

});
