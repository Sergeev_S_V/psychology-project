import merge from 'xtend';
import reducer from '../store/reducers/notificationsReducer';
import {initialState} from '../store/reducers/notificationsReducer';
import {
    FETCH_NOTIFICATIONS_FAILURE, FETCH_NOTIFICATIONS_SUCCESS,
    GET_IMPORTANT_USERS_FAILURE
} from "../store/actions/actionTypes";

describe('notifications reducer', () => {

    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });

    it('should handle FETCH_NOTIFICATIONS_SUCCESS', () => {
        const action = {type: FETCH_NOTIFICATIONS_SUCCESS, data: [{userId: '123', sectionId: '456', showed: true, review: false}]};
        const expectedState = merge(initialState,
            {notifications: [{userId: '123', sectionId: '456', showed: true, review: false}]});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle FETCH_NOTIFICATIONS_FAILURE', () => {
        const action = {type: FETCH_NOTIFICATIONS_FAILURE, error: 'FETCH_NOTIFICATIONS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'FETCH_NOTIFICATIONS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle GET_IMPORTANT_USERS_FAILURE', () => {
        const action = {type: GET_IMPORTANT_USERS_FAILURE, error: 'GET_IMPORTANT_USERS_FAILURE'};
        const expectedState = merge(initialState,
            {error: 'GET_IMPORTANT_USERS_FAILURE'});
        expect(reducer(initialState, action)).toEqual(expectedState);
    });

});