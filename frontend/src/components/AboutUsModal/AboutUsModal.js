import React from "react";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";

const AboutUsModal = props => {
    const data = localStorage.getItem("firstVisit");
    return data === "false" ? null : (
        <Dialog
            open={props.isOpen}
            onClose={props.handleModalChange}
            scroll={"body"}
            aria-labelledby="scroll-dialog-title"
        >
            <DialogTitle id="scroll-dialog-title">О чем и зачем этот сайт?</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Каждый из нас был или находится в периоде подросткового возраста. Подростковый (пубертатный) период
                    имеет свои особенности в силу происходящих в молодом организме гормональных изменений. Психологи
                    INSIGHT составили Курс развития личности для подростков, который состоит из 4 модулей (Эмоции и
                    здоровье, Самооценка и Образ тела, Стресс и конфликты, Целеполагание и навыки коммуникации) и
                    охватывает самые актуальные вопросы и проблемы подростков. Курс показал свою эффективность при живом
                    групповом проведении.
                    Поэтому мы решили разработать интерактивный сайт, на котором этот курс представлен. Этот Курс не
                    заменяет полноценной психологической консультации, но может стать личным помощником для любого
                    молодого человека от 13 и старше. Вы можете оценить эффективность Курса сами непосредственно,
                    поскольку у вас есть возможность пройти диагностику до и после курса прямо на сайте.
                    Вся информация, получаемая нами, находится в режиме конфиденциальности. Подробнее вы можете узнать
                    из Политики конфиденциальности и Условия пользования.
                    Мы планируем улучшать сайт и развивать его. Будем рады получить отзывы, замечания, пожелания по
                    электронной почте: info@insight.com.kg
                    INSIGHT благодарит команду IT-специалистов, выпускников IT – Attractor School, которые разработали
                    сайт на бесплатной основе.
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={props.handleModalChange} color="primary">
                    Хорошо :)
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default AboutUsModal;
