module.exports = function (teg, searchParam, name, time = 10000) {
  if (searchParam === 'text') {
    browser.element(`//${teg}[${searchParam}()='${name}']`);
    browser.waitForVisible(`//${teg}[${searchParam}()='${name}']`, time);
    browser.click(`//${teg}[${searchParam}()='${name}']`);
  } else {
    browser.waitForVisible(`//${teg}[@${searchParam}='${name}']`, time);
    browser.element(`//${teg}[@${searchParam}='${name}']`);
    browser.click(`//${teg}[@${searchParam}='${name}']`);
  }
};
