module.exports = function (teg, searchParam, name, text, time = 10000) {
  browser.element(`//${teg}[@${searchParam}='${name}']`);
  browser.waitForVisible(`//${teg}[@${searchParam}='${name}']`, time);
  browser.setValue(`//${teg}[@${searchParam}='${name}']`, text);
};
