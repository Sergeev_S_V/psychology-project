const baseUrl = 'http://localhost:3010';

module.exports = {
    mainUrl: baseUrl,
    adminUrl: baseUrl + '/admin',
    registerUrl: baseUrl + '/register',
    loginUrl: baseUrl + '/login',
  finishedSections: baseUrl + '/admin/users/finished-sections',
  allUsers: baseUrl + '/users'
};