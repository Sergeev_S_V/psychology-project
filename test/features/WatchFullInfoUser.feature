#language: ru
#@myTest
Функционал: Просмотр информации о пользователях
  Предыстория:
    Допустим я разлогинен
    Допустим я вхожу на сайт как "user1@mail.ru" c поролем "user1"
    Допустим прохожу секцию
    Допустим я разлогинен
    Допустим я вхожу на сайт как "user2@mail.ru" c поролем "user2"
    Допустим я написал рецензию для секции
    Допустим я вижу уведомление что рецензия была отправлена
  Сценарий:
    Если я перехожу на роут со всеми пользователями
    И нажимаю на карточку пользователя
    То я вижу информацию об этом пользователе
    И я разлогинен