#language: ru

Функционал: Просмотр списка важных пользователей
  Предыстория:
    Допустим я разлогинен
  Сценарий:
    Допустим я вхожу на сайт как "user2@mail.ru" c поролем "user2"
    Если перехожу на страницу со всеми пользователями с важными ответами
    То я вижу всех пользователей
    И я разлогинен